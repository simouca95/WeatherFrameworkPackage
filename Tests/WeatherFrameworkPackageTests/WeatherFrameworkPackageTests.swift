import XCTest
@testable import WeatherFrameworkPackage

final class WeatherFrameworkPackageTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(WeatherFrameworkPackage().text, "Hello, World!")
    }
}
